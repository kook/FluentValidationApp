﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentValidation.Results;
using FluentValidationAPP.Models;

namespace FluentValidationAPP.Controllers
{
    public class CustomerController : Controller
    {
        //FluentValidation验证更多：
        //http://fluentvalidation.codeplex.com/
        //http://www.cnblogs.com/libingql/p/3801704.html
        // GET: /Customer/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            CustomerValidator validator = new CustomerValidator();
            ValidationResult result = validator.Validate(customer);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(error =>
                {
                    ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                });
            }

            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            return View(customer);
        }
    }
}

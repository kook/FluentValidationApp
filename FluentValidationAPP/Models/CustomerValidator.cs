﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
namespace FluentValidationAPP.Models
{
    public class CustomerValidator : AbstractValidator<Customer>
    {
        public CustomerValidator()
        {
            RuleFor(customer => customer.CustomerName).NotNull().WithMessage("客户名称不能为空");
            RuleFor(customer => customer.Email)
                .NotEmpty().WithMessage("邮箱不能为空")
                .EmailAddress().WithMessage("邮箱格式不正确");
            RuleFor(customer => customer.Discount)
                .NotEqual(0)
                .When(customer => customer.HasDiscount);
            RuleFor(customer => customer.Address)
                .NotEmpty()
                .WithMessage("地址不能为空")
                .Length(20, 50)
                .WithMessage("地址长度范围为20-50字节");
        }
    }
}
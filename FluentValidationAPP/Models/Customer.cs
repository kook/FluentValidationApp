﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FluentValidationAPP.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public float? Discount { get; set; }
        public bool HasDiscount { get; set; }
    }
}